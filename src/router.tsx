import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './screens/home';
import Profile from './screens/profile';
import { stackNavParamList } from './Common/typeDefs';
import { StatusBar } from 'react-native';

const Stack = createNativeStackNavigator<stackNavParamList>();

function App(){
    return(
        <NavigationContainer>
            <StatusBar barStyle='dark-content' backgroundColor='white'/>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Profile" component = {Profile} options={{headerShown:false}}/>   
            </Stack.Navigator> 
        </NavigationContainer>
    )
}

export default App;