import axios from 'axios';

const Api = (
  type: string,
  serviceUrl: string,
  params?: object | null,
  data?: object | null,
) => {
  axios.defaults.headers.common['Accept'] = 'application/json';
  axios.defaults.headers.common['Content-Type'] = 'application/json';
  const url =serviceUrl;
  switch (type) {
    case 'get':
      return axios
        .get(url, {params: params})
        .then(response => {
          __DEV__ && console.log('Get Api Success: ', url, params, response);
          return response?.data ?? response;
        })
        .catch(error => {
          __DEV__ && console.log('Get Api Error : ', url, params, error);
          return error;
        });

    case 'post':
      return axios
        .post(url, params, {params: data, maxContentLength: Infinity})
        .then(response => {
          __DEV__ && console.log('Post Api Success: ', url, params, response);
          return response;
        })
        .catch(error => {
          __DEV__ && console.log('Post Api Error : ', url, params, error);
          return error;
        });
    default:
      break;
  }
};

export default Api;
