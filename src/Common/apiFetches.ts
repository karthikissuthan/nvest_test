import Api from "./api"
const key = '5fDzO42stIQJ4WDQOWXMK43KbEZotuXB3JVquZlvF1KjVcMH8R3aJMvJGEVvlIIK';
const keyappend='&api_key='+key;

export const GetTokenList = () => {
    return Api('get', 'https://min-api.cryptocompare.com/data/all/coinlist')
        ?.then(result => {
            if (result && result?.Response === 'Success') {
                if (typeof result?.Data === 'object') {
                    const temp = Object.values(result.Data);
                    temp.sort((a, b) => parseInt(a?.SortOrder) - parseInt(b?.SortOrder))
                    return temp;
                }
            }

        })
}

export const GetTokenData = (params?: object) => {
    const param = {
        ...params,
        tsym: 'USD',
        api_key: key
    }
    return Api('get', 'https://min-api.cryptocompare.com/data/top/mktcapfull', param)
        ?.then(result => {
            console.log(result);
            if (result && result?.Message === 'Success') {
                return result;
            }

        })
}

export const GetTokenChartData = (Abbr: string) => {
    return Api('get', `https://min-api.cryptocompare.com/data/v2/histoday?fsym=${Abbr}&tsym=USD&limit=10`+keyappend)
        ?.then(result => {
            if (result?.Response === 'Success') {
                return result?.Data;
            }
        })
}