import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CustomFonts from '../Assets/Fonts';
import Images from '../Assets/Images';

const Header = (props: {
  myBalance: boolean;
  name?: string;
  navigation: any;
}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={()=>props.navigation.goBack()}>
        <Image
          source={props.myBalance ? Images.profile : Images.arrow}
          resizeMode="contain"
          style={[styles.image, !props.myBalance && styles.back]}
        />
      </TouchableOpacity>
      <Text style={styles.text}>
        {props.myBalance ? 'My balance' : props.name}
      </Text>
      <View style={styles.bellWrap}>
        <Image source={Images.bell} style={styles.bell} />
        <View style={styles.countWrap}>
          <View style={styles.countWrapInner}>
            <Text style={styles.count}>3</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  image: {
    height: 35,
    width: 35,
    borderRadius: 45 / 2,
  },
  back: {
    height: 30,
    width: 30,
    tintColor: '#5D6D7E',
    marginBottom:5
  },
  text: {
    fontFamily: CustomFonts.BoldFont,
    fontSize: 16,
    color: '#5D6D7E',
    lineHeight: 18,
    // fontWeight:'bold'
  },
  bellWrap: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  bell: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    tintColor: '#D7DBDD',
  },
  countWrap: {
    position: 'absolute',
    bottom: 13,
    left: 13,
    padding: 2,
    backgroundColor: 'white',
    borderRadius: 17,
  },
  countWrapInner: {
    backgroundColor: '#40dd9c',
    height: 15,
    width: 15,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  count: {
    fontSize: 10,
    fontFamily: CustomFonts.SemiboldFont,
    color: 'white',
  },
});
