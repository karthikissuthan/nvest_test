import React from 'react';
import {Dimensions, Image, StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import CustomFonts from '../Assets/Fonts';
import Images from "../Assets/Images";
import { LineChart} from 'react-native-chart-kit';

const Chart = (props:{data:Array<number> , source:'home'|'detail' , label?:string[]})=>{
  const height = props.source==='home'?90:250;
  const sub = props.source==='home'?-15:30;
  const translate = props.source === 'home' ? 10 : 15;
  const translateY = props.source === 'home' ? -18 : 0;
    return (
      <LineChart
        data={{
          labels:props.label ?? [],
          datasets: [
            {
              data: props.data,
            },
          ],
        }}
        width={Dimensions.get('window').width - sub} 
        height={height}
        yAxisInterval={1} 
        withInnerLines={false}
        withOuterLines={false}
        withHorizontalLabels={false}
        withVerticalLabels={props.source === 'detail'}
        getDotColor={(datapoint, index) =>
          index === 10 && props.source==='detail' ? 'rgba(188,147,253,0.3)' : 'transparent'
        }
        chartConfig={{
          backgroundColor: '#FFFFFF',
          backgroundGradientFrom: '#FFFFFF',
          backgroundGradientTo: '#FFFFFF',
          fillShadowGradientFromOpacity: props.source==='home'?0.3:0.1,
          fillShadowGradientToOpacity: 0,
          decimalPlaces: 2, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(188, 147, 253, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(113, 121, 126, ${opacity})`,
          style: {
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
          },
        }}
        style={{
          alignSelf: 'center',
          paddingRight: 0,
          transform: [{translateX: translate},{translateY:translateY}],
        }}
      />
    );
}

export default Chart;

const styles = StyleSheet.create({
 
});