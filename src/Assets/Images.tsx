const Images = {
  bell: require('./Images/bell.png'),
  profile: require('./Images/profile.png'),
  arrow: require('./Images/arrow.png'),
  zigzag: require('./Images/zigzag.png'),
  bar: require('./Images/bar.png'),
};
export default Images;