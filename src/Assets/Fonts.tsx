const CustomFonts = {
    BlackFont: 'Poppins-Black',
    BlackItalicFont: 'Poppins-BlackItalic',
    RegularFont: 'Poppins-Regular',
    BoldFont: 'Poppins-Bold',
    BoldItalicFont: 'Poppins-BoldItalic',
    ExtraBoldFont: 'Poppins-ExtraBold',
    ExtraBoldItalicFont: 'Poppins-ExtraBoldItalic',
    ExtraLightFont: 'Poppins-ExtraLight',
    ExtraLightItalicFont: 'Poppins-ExtraLightItalic',
    ItalicFont: 'Poppins-Italic',
    LightFont: 'Poppins-Light',
    LightItalicFont: 'Poppins-LightItalic',
    SemiboldFont: 'Poppins-SemiBold',
    SemiboldItalicFont: 'Poppins-SemiBoldItalic',
    MediumFont: 'Poppins-Medium',
    MediumItalicFont: 'Poppins-MediumItalic',
    ThinFont: 'Poppins-Thin',
    ThinItalicFont: 'Poppins-ThinItalic',
}

export default CustomFonts;