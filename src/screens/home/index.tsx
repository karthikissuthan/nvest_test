import React, {memo, useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import CustomFonts from '../../Assets/Fonts';
import {
  GetTokenChartData,
  GetTokenData,
  GetTokenList,
} from '../../Common/apiFetches';
import Header from '../../components/header';
import LineChart from '../../components/chart';
import {styles} from './styles';

const App = ({navigation, route}: any) => {
  const [displayData, setDisplayData] = useState<any[]>([]);
  const [fullDataLength, setDataLength] = useState(0);
  const [displayDataLength, setDisplayLength] = useState(0);
  const [page, setPage] = useState(2);

  useEffect(() => {
    getData();
  }, []);

  const getData = (limit = 30, page = 0) => {
    const param = {
      limit,
      page,
    };
    GetTokenData(param)?.then(result => {
      result?.MetaData?.Count && setDataLength(result?.MetaData?.Count);
      if (Array.isArray(result?.Data)) {
        setDisplayLength(old => old + result?.Data.length);
        setDisplayData(oldArr => [...oldArr, ...result?.Data]);
      }
    });
  };

  const renderList = ({item, index}: {item: any; index: number}) => {
    return <Child item={item} index={index} navigation={navigation} />;
  };

  const handleEndReached = () => {
    if (displayDataLength < fullDataLength) {
      getData(10, page + 1);
      setPage(page + 1);
    }
  };

  const ListHeaderComponent = useCallback(
    () => (
      <View style={styles.balanceWrap}>
        <Text style={styles.balanceAmt}>
          $<Text style={styles.balanceAmtInner}>2,564.25</Text>
        </Text>
        <Text style={styles.updatedText}>
          Updated{' '}
          <Text style={styles.updatedTextInner}> 20.03.2019 | 11:31</Text>
        </Text>
      </View>
    ),
    [],
  );

  const listEmptyComponent = () => (
    <View style={styles.empty}>
      <Text style={styles.listEmpty}>Unable to Fetch data</Text>
    </View>
  );

  const ListFooterComponent = () => {
    return displayDataLength < fullDataLength ? (
      <ActivityIndicator style={{margin: 15}} size="large" color="40dd9c" />
    ) : null;
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header myBalance navigation={navigation} />
      <View style={styles.listWrap}>
        <FlatList
          data={displayData}
          renderItem={renderList}
          keyExtractor={(_, index) => index.toString()}
          contentContainerStyle={{flexGrow:1}}
          style={{elevation: 5}}
          ListHeaderComponent={ListHeaderComponent}
          ListFooterComponent={ListFooterComponent}
          onEndReached={handleEndReached}
          onEndReachedThreshold={1}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={listEmptyComponent}
        />
      </View>
    </SafeAreaView>
  );
};

const Child = memo((props: {item: object; index: number; navigation: any}) => {
  const {FullName, Name, ImageUrl} = props?.item?.CoinInfo;
  const PRICE = props?.item?.DISPLAY?.USD?.PRICE ?? '';
  const CHANGEPCTHOUR = props?.item?.DISPLAY?.USD?.CHANGEPCTHOUR ?? '';
  const [chartData, setChartData] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    GetTokenChartData(Name)?.then(result => {
      if (Array.isArray(result?.Data)) {
        setChartData(result?.Data?.map(i => i.close));
        setLoading(false);
      }
    });
  }, []);

  return (
    <TouchableOpacity
      style={styles.tile}
      onPress={() =>
        props.navigation.navigate('Profile', {
          item: props.item,
        })
      }>
      <View style={styles.tileheaderContainer}>
        <View style={styles.coinNameContainer}>
          <Image
            source={{uri: 'https://www.cryptocompare.com' + ImageUrl}}
            style={styles.coinIcon}
          />
          <View style={styles.coinNameTextWrap}>
            <Text style={styles.coinName}>{FullName}</Text>
            <Text style={styles.coinAbbr}>{Name}</Text>
          </View>
        </View>
        <View style={styles.coinPriceWrap}>
          <Text style={styles.coinPrice}>{PRICE}</Text>
          <Text
            style={[
              styles.coinPriceChange,
              CHANGEPCTHOUR != '0.00' && {
                color: CHANGEPCTHOUR > 0 ? '#40dd9c' : 'red',
              },
            ]}>{`${CHANGEPCTHOUR > 0 ? '+' : ''}${CHANGEPCTHOUR}`}</Text>
        </View>
      </View>
      {loading ? (
        <View style={{height: 40, width: '100%'}}></View>
      ) : (
        <LineChart data={chartData} source={'home'} />
      )}
    </TouchableOpacity>
  );
});

export default App;
