import { StyleSheet } from "react-native";
import CustomFonts from "../../Assets/Fonts";

export const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#fdfdff',
    // padding: 15,
  },
  balanceAmt: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#36454F',
    fontFamily: CustomFonts.BoldFont,
  },
  balanceAmtInner: {
    fontSize: 28,
    fontFamily: CustomFonts.BoldFont,
    // lineHeight:35,
  },
  updatedText: {
    fontSize: 12,
    color: '#AAB7B8',
    fontFamily: CustomFonts.SemiboldFont,
  },
  updatedTextInner: {
    color: '#7F8C8D',
    fontFamily: CustomFonts.SemiboldFont,
  },
  balanceWrap: {
    alignItems: 'center',
    marginTop: 30,
    marginBottom:25,
  },
  listWrap: {
    flex: 1,
    // backgroundColor:'pink',
  },
  tile: {
    flex: 1,
    borderRadius: 10,
    backgroundColor: 'white',
    marginBottom: 10,
    elevation: 10,
    marginHorizontal: 15,
    shadowColor: '#7393B3',
    overflow:'hidden',
    height:150,
  },
  tileheaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    // backgroundColor:'pink'
  },
  coinNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinIcon: {
    height: 45,
    width: 45,
    resizeMode: 'contain',
  },
  coinNameTextWrap: {
    marginLeft: 10,
  },
  coinName: {
    fontFamily: CustomFonts.BoldFont,
    fontSize: 14,
    lineHeight: 16,
    color: '#71797E',
  },
  coinAbbr: {
    fontFamily: CustomFonts.SemiboldFont,
    fontSize: 12,
    lineHeight: 14,
    color: '#A9A9A9',
  },
  coinPriceWrap: {},
  coinPrice: {
    fontFamily: CustomFonts.BoldFont,
    color: '#71797E',
    fontSize: 14,
    lineHeight: 16,
  },
  coinPriceChange: {
    fontFamily: CustomFonts.SemiboldFont,
    fontSize: 12,
    lineHeight: 14,
    color: 'blue',
    textAlign: 'right',
  },
  listEmpty:{
    // fontSize:25,
    color:'black'
  },
  empty:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  }
});