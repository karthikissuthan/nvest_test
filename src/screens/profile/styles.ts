import { StyleSheet } from "react-native";
import CustomFonts from "../../Assets/Fonts";

export const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#fdfdff',
    // padding: 15,
  },
  balanceAmt: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#36454F',
    fontFamily: CustomFonts.BoldFont,
  },
  balanceAmtInner: {
    fontSize: 28,
    fontFamily: CustomFonts.BoldFont,
    // lineHeight:35,
  },
  updatedText: {
    fontSize: 12,
    color: '#AAB7B8',
    fontFamily: CustomFonts.SemiboldFont,
  },
  updatedTextInner: {
    // color: '#7F8C8D',
    fontFamily: CustomFonts.SemiboldFont,
    color: 'blue',
  },
  balanceWrap: {
    alignItems: 'center',
    marginTop: 30,
  },
  listWrap: {
    flex: 1,
    // backgroundColor:'pink',
  },
  coinIcon: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
    marginBottom: 10,
  },
  tabWrap: {
    width: 50,
    backgroundColor: '#EFF0F0',
    borderColor: '#EFF0F0',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 2,
    borderRadius: 8,
    borderWidth: 1,
  },
  tabText: {
    fontFamily: CustomFonts.SemiboldFont,
    fontSize: 10,
    color:'#5D6D7E',
  },
  tabContainer: {
    width: '100%',
    paddingVertical: 30,
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerContainer: {
    flexDirection: 'row',
    marginHorizontal: 15,
    justifyContent: 'space-around',
    paddingTop: 25,
    flex: 1,
    alignItems: 'center',
  },
  footerWrap: {
    alignItems: 'center',
  },
  footerHeading: {
    fontFamily: CustomFonts.BoldFont,
    fontSize: 14,
    lineHeight: 16,
    color: '#71797E',
  },
  footertext: {
    fontSize: 10,
    color: '#AAB7B8',
    fontFamily: CustomFonts.SemiboldFont,
  },
  buttonWrap: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    width: '80%',
    alignSelf: 'center',
    justifyContent: 'space-evenly',
    margin: 25,
  },
  button: {
    backgroundColor: '#fd7294',
    height: 50,
    width: '45%',
    borderRadius: 10,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontFamily: CustomFonts.BoldFont,
    color: 'white',
    fontSize: 14,
  },
  green: {
    backgroundColor: '#40dd9c',
  },
  icon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    tintColor: '#AAB7B8',
    marginLeft: 10,
  },
  iconWrap: {
    flexDirection: 'row',
    marginHorizontal: 15,
  },
  blue: {
    tintColor: '#6894ff',
  },
  value:{
    alignSelf:'flex-end',
    fontFamily: CustomFonts.BoldFont,
    fontSize: 12,
    lineHeight: 14,
    color: '#71797E',
    marginRight:30,
  },
});
