import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Images from '../../Assets/Images';
import {GetTokenChartData, GetTokenList} from '../../Common/apiFetches';
import LineChart from '../../components/chart';
import Header from '../../components/header';
import {styles} from './styles';

const dummy = ['Day', 'Week', 'Month', 'Year', 'All'];

const App = ({navigation, route}: any) => {
  const item = route?.params?.item;
  console.log(item?.DISPLAY?.USD);
  const amt24Hr = item?.DISPLAY?.USD?.CHANGE24HOUR;
  const pcnt24Hr = item?.DISPLAY?.USD?.CHANGEPCT24HOUR;
  const high24Hr = item?.DISPLAY?.USD?.HIGH24HOUR;
  const low24Hr = item?.DISPLAY?.USD?.LOW24HOUR;
  const [chartData, setChartData] = useState<any[]>([]);
  const [labelData, setLabelData] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [selected, setSelected] = useState(0);

  useEffect(() => {
    GetTokenChartData(item?.CoinInfo?.Name)?.then(result => {
      if (Array.isArray(result?.Data)) {
        setChartData(result?.Data?.map(i => i.close));
        setLabelData(result?.Data?.map((_, i) => i));
        setLoading(false);
      }
    });
  }, []);

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView>
        <Header
          myBalance={false}
          name={item?.CoinInfo?.FullName}
          navigation={navigation}
        />
        <View style={styles.balanceWrap}>
          <Image
            source={{
              uri: 'https://www.cryptocompare.com' + item?.CoinInfo?.ImageUrl,
            }}
            style={styles.coinIcon}
          />
          <Text style={styles.balanceAmt}>
            {'$ '}
            <Text style={styles.balanceAmtInner}>
              {item?.DISPLAY?.USD?.PRICE?.slice(2)}
            </Text>
          </Text>
          <Text style={styles.updatedText}>
            {amt24Hr + ' '}
            <Text
              style={[
                styles.updatedTextInner,
                pcnt24Hr != '0.00' && {
                  color: parseInt(pcnt24Hr) > 0 ? '#40dd9c' : 'red',
                },
              ]}>{` ${pcnt24Hr > 0 ? '+' : ''} ${pcnt24Hr}%`}</Text>
          </Text>
        </View>
        <View style={styles.tabContainer}>
          {dummy.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={[
                styles.tabWrap,
                selected === index && {backgroundColor: '#FFFFFF'},
              ]}
              onPress={() => setSelected(index)}>
              <Text style={styles.tabText}>{item}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.iconWrap}>
          <Image source={Images.bar} style={styles.icon} />
          <Image source={Images.zigzag} style={[styles.icon, styles.blue]} />
        </View>
        <View style={{}}>
          <Text style={styles.value}>{item?.DISPLAY?.USD?.PRICE}</Text>
          {loading ? (
            <View style={{height: 250, width: '100%'}}></View>
          ) : (
            <LineChart data={chartData} label={labelData} source="detail" />
          )}
        </View>
        <View style={styles.footerContainer}>
          <View style={styles.footerWrap}>
            <Text style={styles.footerHeading}>{pcnt24Hr}</Text>
            <Text style={styles.footertext}>24h change</Text>
          </View>
          <View style={styles.footerWrap}>
            <Text style={styles.footerHeading}>{high24Hr}</Text>
            <Text style={styles.footertext}>↗ 24h highest</Text>
          </View>
          <View style={styles.footerWrap}>
            <Text style={styles.footerHeading}>{low24Hr}</Text>
            <Text style={styles.footertext}>↙ 24 lowest</Text>
          </View>
        </View>
        <View style={styles?.buttonWrap}>
          <View style={[styles.button, styles.green]}>
            <Text style={styles.buttonText}>↗ BUY</Text>
          </View>
          <View style={styles.button}>
            <Text style={styles.buttonText}>↙ SELL</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;

